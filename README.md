# Progressive Web App - Notifications

Integration with Firebase Cloud Messaging (FCM).

## Installation
1. Enable module.
2. Create your project [https://console.firebase.google.com/](https://console.firebase.google.com/).
3. Get your access crentials from Settings => Cloud Messaging tab to Firebase Settings(/admin/config/system/firebase)

### Composer
If your site is [managed via Composer](https://www.drupal.org/node/2718229), use Composer to
download the module:
   ```sh
   composer require "drupal/pwa_firebase_notification"
   ```
